<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url()?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-cloud-upload-alt"></i>
		</div>
		<div class="sidebar-brand-text mx-3">eLibrary <sup>wb</sup></div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item active">
		<a class="nav-link upload-file" href="<?= base_url('doc/create')?>">
		<i class="fas fa-file-upload"></i>
		<span>Upload File</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">
		eLibrary
	</div>

	<!-- Nav Item - Charts -->
	<li class="nav-item active">
	<a class="nav-link" href="<?= base_url('doc/public');?>">
		<i class="fas fa-users"></i>
		<span>Public</span></a>
	</li>

	<!-- Nav Item - Charts -->
	<li class="nav-item">
	<a class="nav-link" href="<?= base_url('doc/category');?>">
		<i class="fas fa-tags"></i>
		<span>Categorized</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#doc" aria-expanded="true" aria-controls="doc">
			<i class="fas fa-fw fa-folder-open"></i>
			<span>Document</span>
		</a>
		<div id="doc" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Document</h6>
				<a class="collapse-item" href="<?= base_url('doc/my_doc');?>">My Document</a>
				<a class="collapse-item" href="<?= base_url('doc/doc_related');?>">Document Related</a>
			</div>
		</div>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	


	<!-- Heading -->
	<div class="sidebar-heading">
		Community Monitoring
	</div>
	<!-- Nav Item - Charts -->
	<li class="nav-item">
	<a class="nav-link" href="<?= base_url('project/doc');?>">
		<i class="fas fa-project-diagram"></i>
		<span>Project's Doc</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<i class="fas fa-fw fa-database"></i>
			<span>Master</span>
		</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Data Master</h6>
				<a class="collapse-item" href="<?= base_url('admin/master/role');?>">Role</a>
				<a class="collapse-item" href="<?= base_url('admin/master/workflow');?>">Document Workflow</a>
				<a class="collapse-item" href="<?= base_url('admin/master/user');?>">User</a>
			</div>
		</div>
	</li>
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
	<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>