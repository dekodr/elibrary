<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Role</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - Create Role</h6>
		</div>
		<div class="card-body row">
			<div class="col-lg-7">
				<form method="post" action="<?= base_url('admin/master/role/create'); ?>" class="user" accept-charset="utf-8">
					<div class="form-group">
						<label for="">Nama Role</label>
						<input name="name" type="text" class="form-control" placeholder="Nama Role" required>
					</div>

					<div class="form-group">
						<label for="">Deskripsi Role</label>
						<textarea class="form-control" placeholder="Deskripsi Role" name="description" ></textarea>
					</div>

					<div class="form-button-sec">
						<hr>
						<!-- Back button -->
						<a href="<?= base_url('admin/master/role') ?>" class="btn btn-secondary btn-icon-split">
							<span class="icon text-white-50">
								<i class="fas fa-arrow-left"></i>
							</span>
							<span class="text">Cancel</span>
						</a>
						<!-- Save Button -->
						<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
							<span class="icon text-white-50">
								<i class="fas fa-save"></i>
							</span>
							<span class="text">Save Data</span>
						</button>
					</div>

				</form>
			</div>

			<div class="col-lg-5">
				<img class="form-bg" src="<?= base_url('assets/img/bg/role.png');?>">
			</div>
		</div>
	</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>

<?= $this->endSection() ?> ?>