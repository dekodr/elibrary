<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">User</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - New User</h6>
		</div>
		<div class="card-body row">
			<div class="col-lg-7">
				<form method="post" autocomplete="off" action="<?= base_url('admin/master/user/create'); ?>" class="user" accept-charset="utf-8">
					<div class="form-group">
                    <label for="">Role</label>
						<?= form_dropdown('id_role', $option_role, null, 'class="form-control" required');?>
					</div>

					<div class="form-group">
                    <label for="">Nama User</label>
						<input name="name" type="text" class="form-control" placeholder="Full Name" required>
					</div>

					<div class="form-group">
                    <label for="">Email</label>
						<input name="email" autocomplete="off" type="email" class="form-control" placeholder="Email" required>
					</div>

					<div class="form-group">
                    <label for="">Posisi</label>
						<input name="position" type="text" class="form-control" placeholder="Position Detail">
					</div>

					<div class="form-group">
                    <label for="">Password</label>
						<input name="password" type="password" class="form-control" placeholder="User Password" required>
					</div>

					<div class="form-button-sec">
						<hr>
						<!-- Back button -->
						<a href="<?= base_url('admin/master/user') ?>" class="btn btn-secondary btn-icon-split">
							<span class="icon text-white-50">
								<i class="fas fa-arrow-left"></i>
							</span>
							<span class="text">Cancel</span>
						</a>
						<!-- Save Button -->
						<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
							<span class="icon text-white-50">
								<i class="fas fa-save"></i>
							</span>
							<span class="text">Save Data</span>
						</button>
					</div>

				</form>
			</div>

			<div class="col-lg-5">
				<img class="form-bg" src="<?= base_url('assets/img/bg/create-user.png');?>">
			</div>
		</div>
	</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>

<?= $this->endSection() ?> ?>