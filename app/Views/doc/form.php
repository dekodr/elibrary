<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Document</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<?php 
	$test = explode('/', current_url());
	$type = $test[4];

	// Public - Anyone who has the link can access. No sign-in required.
	// Private - Shared with spesific people.

	$options_doc_sharing = array('' => '----- Document Sharing -----',
								1 => 'Dokumen Publik',
								0 => 'Dokumen Terbatas');
								
?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - <?= ($type == 'edit')? 'Edit Document' : 'New Document' ?></h6>
		</div>
		<div class="card-body row">
			<div class="col-lg-7">
				<form method="POST" action="" class="user" accept-charset="utf-8" enctype="multipart/form-data" >
					<div class="form-group">
						<label for="">Document Workflow</label>
						<?= form_dropdown('id_doc_matrix', $option_workflow, $id_doc_matrix, 'class="form-control" required');?>
					</div>
					<div class="form-group">
						<label for="">Nama Document</label>
						<input value="<?= $name?>" name="name" type="text" class="form-control" placeholder="Document Name" required>
					</div>
					<div class="form-group">
						<label for="">Deskripsi Document</label>
						<textarea value="<?= $description?>" name="description" class="form-control" placeholder="Deskripsi" ></textarea>
					</div>
					<div class=" form-group">
						<label for="">Kategori Document</label>
						<input value="<?= $tags?>" id="tags-tagator" type="text" name="tags"
							class="tagator form-control"
							placeholder="Document Category/Tags" required>
							*enter to con
					</div>
					<div class="form-group">
						<label for="">Document Sharing</label>
						<?= form_dropdown('is_public', $options_doc_sharing, $is_public, 'class="form-control" required');?>
					</div>
					<div class="form-group">
						<label for="">File Document</label><br>
						<input type="file" name="doc_file" class="" placeholder="Upload Dokumen" required>
						<br>
						<a href="<?= $doc_file?>" target="_blank">current document</a>
					</div>



					<div class="form-button-sec">
						<hr>
						<!-- Back button -->
						<a href="<?= base_url('admin/master/workflow') ?>" class="btn btn-secondary btn-icon-split">
							<span class="icon text-white-50">
								<i class="fas fa-arrow-left"></i>
							</span>
							<span class="text">Cancel</span>
						</a>
						<!-- Save Button -->
						<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
							<span class="icon text-white-50">
								<i class="fas fa-save"></i>
							</span>
							<span class="text">Save Data</span>
						</button>
					</div>

				</form>
			</div>

			<div class="col-lg-5">
				<img class="form-bg" src="<?= base_url('assets/img/bg/add-files.png');?>">
			</div>
		</div>
	</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>
	<script>
		$( document ).ready(function() {
			// TAGATOR
			// let list_role = <?= $list_role;?>;
			// $('#tags-tagator').tagator('autocomplete', list_role);
		});
	</script>
<?= $this->endSection() ?> ?>