<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><?= $page_title; ?></h1>
	<a href="<?= base_url('/doc/create') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Create New Doc</a>
</div>

<?php if (session()->getFlashdata('success')) { ?>
	<div class="alert alert-success">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('success')) ?>
		</div>
	</div>
<?php } ?>

	<!-- FOLDER AREA -->
	<div class="row">
		<?= $file;?>
	</div>
	<!-- END OF FOLDER AREA -->

<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
	var contextMenu = new contextMenuGenerator({
		menu			: [	['#', 'far fa-eye', 'Preview', 'target="_blank"'],
							['#', 'fa fa-info-circle', 'View Detail', 'class="customization_popup_trigger"']],
		roleMenu		: [	['<?= base_url('doc/edit') ?>', 'fa fa-edit', 'Edit Document'],
							['<?= base_url('doc/remove')?>', 'fa fa-trash', 'Remove Document', 'data-toggle="modal" data-target="#removeModal"']],
		form			: {
							title: 'Review Dokumen',
							fields: [
								{
									type: 'bigdiv',
									label: 'Revisi',
									icon: '/assets/img/icon/reject.png',
									url: base_url+'/doc/revision/',
									description: 'Kembalikan dokumen ke user untuk diperbaiki.'
								},{
									type: 'bigdiv',
									label: 'Perbaiki Dokumen',
									icon: '/assets/img/icon/check.png',
									url: base_url+'/doc/upload_ver/',
									description: 'Upload dokumen yang telah diperbaiki.'
								},{
									type: 'bigdiv',
									label: 'Setujui',
									icon: '/assets/img/icon/approve.png',
									url: base_url+'/doc/approve/',
									description: 'Setujui dokumen'
								}
							],
							button: '',
					},
		data			: <?= $json?>,
		login_id		: <?= $_SESSION['user']['id']?>,
		login_role_id	: <?= $_SESSION['user']['id_role']?>,
	})
</script>
<?= $this->endSection() ?> ?>