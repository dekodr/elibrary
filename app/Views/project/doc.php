<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><?= $page_title; ?></h1>
</div>

<?= $path;?>
<?php if (session()->getFlashdata('success')) { ?>
	<div class="alert alert-success">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('success')) ?>
		</div>
	</div>
<?php } ?>

	<!-- FOLDER AREA -->
	<div class="row">
		<?= $file;?>
	</div>
	<!-- END OF FOLDER AREA -->

<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
	var contextMenu = new contextMenuGenerator({
		menu			: [	['#', 'far fa-eye', 'Preview', 'target="_blank"'],
							['#', 'fa fa-info-circle', 'View Detail', 'class="customization_popup_trigger"'],
							// ['#', 'fa fa-download', 'Download']
							],
		data			: <?= $json?>,
		login_id		: <?= $_SESSION['user']['id']?>,
		login_role_id	: <?= $_SESSION['user']['id_role']?>,
	})
</script>
<?= $this->endSection() ?> ?>