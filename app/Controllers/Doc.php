<?php 
namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\WorkflowModel;
use App\Models\RoleModel;
use App\Models\DocModel;
use App\Models\ApprovalModel;

use CodeIgniter\HTTP\Files\UploadedFile;
use Aws\S3\ObjectUploader;

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class Doc extends BaseController
{

	public $request;
	public $wm; //stands for workflow Model
	public $rm; //stands for role Model
	public $dm; //stands for role Model
	public $approval; //stands for Approval Model
	public $generateTable;
	public $generateFile;

	public function __construct(){
		$request 				= \Config\Services::request();
		$this->wm				= new WorkflowModel();
		$this->rm				= new RoleModel();
		$this->dm				= new DocModel();
		$this->approval			= new ApprovalModel();
		$this->generateTable	= new \CodeIgniter\View\Table();
		$this->generateFile		= new \CodeIgniter\View\File();
	}

	//----------------------------------------------------------------------
	//---------------------			PAGE	--------------------------------
	//----------------------------------------------------------------------

	public function create(){
		$data						= null;
		$data['option_workflow']	= $this->wm->get_data_option();
		$files 						= $this->request->getFiles();
		$files 						= $files['doc_file'];
		$payload 					= $this->request->getPost();

		if($payload){
			$fileName 			= $files->getRandomName();
			$files->move(WRITEPATH . 'uploads/doc/', $fileName); //upload to dir

			$photo_uri			= WRITEPATH . 'uploads/doc/' . $fileName;
			$fileName			= $this->s3($fileName, $photo_uri, "elibrary/document");
			$payload['doc_file']= $fileName;

			$save  = $this->dm->create($payload);

			if ($save) {
				return redirect()->to(base_url('doc/my_doc'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('doc/form', $data);
		}
	}

	public function my_doc(){

		$document			= $this->dm->get_my_data();
		$data['page_title']	= 'My Document';
		$data['file']		= $this->generateFile->generateFile($document);
		$data['json']		= json_encode($document);
		// print_r($document);
		return view('doc/list', $data);
	}

	public function doc_related(){
		$document			= $this->dm->get_data();
		$data['page_title']	= 'Document Related';

		if(count($data) > 0)
			$data['file']  = $this->generateFile->generateFile($document);

		$data['json']		= json_encode($document);
		return view('doc/list', $data);
	}

	public function public($type = null){
		$document			= $this->dm->get_public_doc();
		$data['page_title']	= 'Public Document';

		if(count($data) > 0)
			$data['file']  = $this->generateFile->generateFile($document);

		$data['json']		= json_encode($document);
		return view('doc/list', $data);
	}

	public function category(){
		$q					= $this->request->getGet('q');
		$document			= $this->dm->get_doc_category($q);
		$data['page_title']	= 'Categorized Documents';
		$data['json']		= json_encode($document);

		if((count($data) > 0) && !$q){
			$data['file']	= $this->generateFile->generateFolder($document);
		}else{
			$data['path']	= $this->generateFile->generateBreadcrumb();
			$data['file']	= $this->generateFile->generateFile($document);
		}

		// print_r($document);
		return view('doc/categorized', $data);
	}

	public function revision($id_doc){
		$payload 					= $this->request->getPost();

		if($payload){
			$payload_ = array(	
								'id_document' => $id_doc,
								'description' => 'reject',
								'to_user_owner' => $payload['to_user_owner'],
								'detail' => $payload['description']);
			$save  = $this->dm->revision($payload_);

			if ($save) {
				return redirect()->to(base_url('doc/doc_related'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('doc/revision');
		}
	}

	public function upload_ver($id_doc){
		$files 						= $this->request->getFiles();
		$files 						= $files['doc_file'];

		if($files){
			$fileName 			= $files->getRandomName();
			$files->move(WRITEPATH . 'uploads/doc/', $fileName); //upload to dir

			$photo_uri			= WRITEPATH . 'uploads/doc/' . $fileName;
			$fileName			= $this->s3($fileName, $photo_uri, "elibrary/document");

			$save  = $this->dm->action_doc($id_doc, 'update_ver', $fileName);

			if ($save) {
				return redirect()->to(base_url('doc/doc_related'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('doc/upload-ver');
		}
	}
	
	public function edit($id_doc){
		$data						= json_decode(json_encode($this->dm->get_data($id_doc)), true);
		$data['option_workflow']	= $this->wm->get_data_option();
		$files 						= $this->request->getFiles();
		$files 						= $files['doc_file'];
		$payload 					= $this->request->getPost();

		if($payload){
			$fileName 			= $files->getRandomName();
			$files->move(WRITEPATH . 'uploads/doc/', $fileName); //upload to dir

			$photo_uri			= WRITEPATH . 'uploads/doc/' . $fileName;
			$fileName			= $this->s3($fileName, $photo_uri, "elibrary/document");
			$payload['doc_file']= $fileName;

			$save  = $this->dm->edit($id_doc, $payload);

			if ($save) {
				return redirect()->to(base_url('doc/my_doc'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('doc/form', $data);
		}
	}
	//----------------------------------------------------------------------

	public function approve($id_doc = null){
		if($id_doc !== null){
			$save = $this->dm->action_doc($id_doc, 'approve');
			if ($save) {
				return redirect()->to(base_url('doc/doc_related'))->with('success', 'Approval Success.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble approve your data.');
			}
		}else{
			return FALSE;
		}
	}

	public function remove($id_doc){
		$remove = $this->dm->remove($id_doc);

		if ($remove) {
			return redirect()->to(base_url('doc/my_doc'))->with('success', 'Data successfully saved.');
		} else {
			return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
		}
	}

	public function detail_doc($id){
		$document			= $this->dm->get_data($id);
		return json_encode($document);
	}

	public function s3($key, $uri, $dir){
		$s3 = new S3Client([
			'region'  => 'ap-southeast-1',
			'version' => 'latest',
			'credentials' => [
				'key'    => 'AKIAQDS7UK2EVXQZVK4N',
				'secret' => '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'
			]
		]);

		// Send a PutObject request and get the result object.
		$key = $dir . "/" . $key;

		$result = $s3->putObject([
			'Bucket'        => 'worldbank',
			'Key'           => $key,
			'SourceFile'    => $uri,
			'ACL'           => 'public-read',
		]);

		// Print the body of the result by indexing into the result object.
		// var_dump($result["@metadata"]["effectiveUri"]);

		return $result["@metadata"]["effectiveUri"];
	}

	public function download($data='1610507533_9e250d16293746d1022d.css'){

		$s3 = new S3Client([
			'region'  => 'ap-southeast-1',
			'version' => 'latest',
			'credentials' => [
				'key'    => 'AKIAQDS7UK2EVXQZVK4N',
				'secret' => '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'
			]
		]);
		
		try {
			// Get the object.
			$result = $s3->getObject([
				'Bucket' => 'worldbank',
				'Key'    => 'elibrary/document/'.$keyname
			]);
		
			// Display the object in the browser.
			print_r($result);
			header("Content-Type: {$result['ContentType']}");
			echo $result['Body'];
		} catch (S3Exception $e) {
			echo"jembi";
			echo $e->getMessage() . PHP_EOL;
		}
	}






}
