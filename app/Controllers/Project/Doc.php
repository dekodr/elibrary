<?php 
namespace App\Controllers\Project;

use App\Controllers\BaseController;

use App\Models\CM\CMModel;
class Doc extends BaseController
{

	public $request;
	public $cmm; //stands for workflow Model
	public $generateTable;
	public $generateFile;
	public $key = 'comm';

	public function __construct(){
		// $encrypter 				= \Config\Services::encrypter();
		$this->cmm				= new CMModel();
		$this->generateTable	= new \CodeIgniter\View\Table();
		$this->generateFile		= new \CodeIgniter\View\File();
	}

	public function index(){

		$id_prov			= $this->request->getGet('prov');
		$id_city			= $this->request->getGet('city');
		$id_doc				= $this->request->getGet('doc');
		$data['page_title']	= 'Community Monitoring Document';

		if ($id_city) {
			$document			= $this->cmm->get_doc_city($id_city);
			$data['json']		= json_encode($document);
			$data['file']		= $this->generateFile->generateFolder($document);
			

		}else if ($id_doc) {
			$document			= $this->cmm->get_doc($id_doc);
			$data['json']		= json_encode($document);
			$document = (object)$document;
			$data['file']		= $this->generateFile->generateFile((object)$document);
			

		}else{

			$document			= (!$id_prov)? $this->cmm->get_province() : $this->cmm->get_city($id_prov);
			$data['json']		= json_encode($document);
			$data['file']		= $this->generateFile->generateFolder($document);
		}
		

		return view('project/doc', $data);
	}

	//--------------------------------------------------------------------

}
