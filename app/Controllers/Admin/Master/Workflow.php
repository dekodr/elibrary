<?php 
namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\WorkflowModel;
use App\Models\RoleModel;

class Workflow extends BaseController
{

	public $request;
	public $wm; //stands for workflow Model
	public $rm; //stands for role Model
	public $generateTable;

	public function __construct(){
		$request 				= \Config\Services::request();
		$this->wm				= new WorkflowModel();
		$this->rm				= new RoleModel();
		$this->generateTable	= new \CodeIgniter\View\Table();

	}

	public function index(){
		$data   = $this->wm->get_data();
		
		if(count($data) > 0){
			foreach ($data as $key => $value)
				$data['table'][$key] = array($value->name, $value->approval, '
						<div class="mt-4 mb-2">
							<a href="'.base_url('/admin/master/workflow/edit/'.$value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
							<a href="'.base_url('/admin/master/workflow/delete/'.$value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
						</div>');

			$data['table']  = $this->generateTable->generate($data['table']);
		}

		return view('admin/master/workflow/list', $data);
	}

	public function create(){
		$data				= null;
		$data['list_role']	= $this->get_name_json($this->rm->get_data());

		if(($this->request->getPost())){
			//save to db
			$save  = $this->wm->create($this->request->getPost());

			if ($save) {
				return redirect()->to(base_url('admin/master/workflow'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('admin/master/workflow/form', $data);
		}
	}

	public function get_name_json($data){
		if(count($data) > 0){
			foreach ($data as $key => $value) {
				# code...
				$return[$key] = $value->name;
			}

			return utf8_encode(json_encode($return));
		}else{
			return FALSE;
		}
	}

	public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->wm->save($data);

        return redirect()->to(base_url('admin/master/workflow'))->with('success', 'Workflow successfully deleted.');
    }

    public function edit($id = '')
    {	
            $data = [
                'isEdit' => 'edit',
                'getWorkflow' => $this->wm->get_data($id),
                'list_role' => $this->get_name_json($this->rm->get_data())
            ];
            // print_r($this->wm->get_data($id));die;
            return view('admin/master/workflow/edit', $data);
    }

    public function save_update()
    {
        $data = $this->request->getVar();

        $data['id'] = $data['id'];
        $data['edit_date'] = date('Y-m-d H:i:s');

        $save = $this->wm->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/workflow'))->with('success', 'Workflow successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }
	//--------------------------------------------------------------------

}
