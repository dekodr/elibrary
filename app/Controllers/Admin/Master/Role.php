<?php 
namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\RoleModel;

class Role extends BaseController
{

	public $request;
	public $rm; //stands for role Model
	public $generateTable;

	public function __construct(){
		$request 				= \Config\Services::request();
		$this->rm				= new RoleModel();
		$this->generateTable	= new \CodeIgniter\View\Table();

	}

	public function index()
	{
		$data   = $this->rm->get_data();

		foreach ($data as $key => $value)
			$data['table'][$key] = array($value->name, $value->description, '
					<div class="mt-4 mb-2">
						<a href="'.base_url('/admin/master/role/edit/'.$value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
						<a href="'.base_url('/admin/master/role/delete/'.$value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
					</div>');

		$data['table']	= $this->generateTable->generate($data['table']);
		
		return view('admin/master/role/list', $data);
	}

	public function create()
	{
		// print_r($this->request->getPost());

		if(($this->request->getPost())){
			//save to db
			$save  = $this->rm->create($this->request->getPost());

			if ($save) {
				return redirect()->to(base_url('admin/master/role'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('admin/master/role/form');
		}
	}

	public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->rm->save($data);

        return redirect()->to(base_url('admin/master/role'))->with('success', 'Role successfully deleted.');
    }

    public function edit($id = '')
    {
            $data = [
                'isEdit' => 'edit',
                'getRole' => $this->rm->get_data($id)
            ];
            return view('admin/master/role/edit', $data);
    }

    public function save_update()
    {
        $data = $this->request->getVar();

        $data['id'] = $data['id'];
        $data['edit_date'] = date('Y-m-d H:i:s');

        $save = $this->rm->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/role'))->with('success', 'Role successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }
	//--------------------------------------------------------------------

}
