<?php

namespace App\Models\CM;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class CMModel extends Model
{
	public $db;

	public function __construct(){
		$this->db		= db_connect('comm_monitoring');
	}

	// get published document project
	public function get_data(){
		$data 		= $this->db->table('ms_report')
								->select('ms_report.*,
										ms_user.name,
										ms_provinsi.name province,
										ms_kabupaten.name kabupaten')
								->join('ms_user', 'ms_user.id = ms_report.id_user')
								->join('ms_provinsi', 'ms_provinsi.id = ms_user.id_province', 'LEFT')
								->join('ms_kabupaten', 'ms_kabupaten.id = ms_user.id_city', 'LEFT')
								->where('is_publish', true)
								->get()->getResult();

		foreach ($data as $key => $value){
			$_data[$value->province][]			= $value;
		}

		return $_data;

	}

	public function get_province(){

		$data	= $this->db->table('ms_provinsi')
							->where('delete', 0)
							->get()->getResult();
		$_data	= null;

		foreach ($data as $key => $value){
			$_data[$key]		= (array) $value;
			$_data[$key]['url']	= base_url('project/doc?prov='.$value->id);
		}
		return $_data;
	}
	
	public function get_city($id_province = null){

		$data 		= $this->db->table('ms_kabupaten')
								->where('id_province', $id_province)
								->where('delete', 0)
								->get()->getResult();
		
		$_data	= null;

		foreach ($data as $key => $value){
			$_data[$key]		= (array) $value;
			$_data[$key]['url']	= base_url('project/doc?prov='.$id_province.'&city='.$value->id);
		}
		return $_data;
	}

	public function get_doc_city($id_city = null){

		$data 		= $this->db->table('ms_report')
								->select('ms_report.*, ms_ksm.name ksm')
								->join('ms_user', 'ms_user.id = ms_report.id_user')
								->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
								->where('id_city', $id_city)
								->where('report_type', null)
								->where('ms_report.delete', 0)
								->get()->getResult();
		
		$_data	= null;

		foreach ($data as $key => $value){
			$title = ($value->title == 'foto_progress') ? 'Laporan Infrastruktur' : 'Laporan Manajemen Keuangan';
			$_data[$key]				= (array) $value;
			$_data[$key]['name']		= 'KSM '.$value->ksm.' - '.$title.' '.$value->progress.'%';
			$_data[$key]['url']			= base_url('project/doc?doc='.$value->idoffline);
		}
		
		return $_data;
	}

	function get_doc($id = null){
		$_data	= array();
		$data	= $this->db->table('ms_report')
							->select('ms_report.*, ms_user.name user_owner')
							->join('ms_user', 'ms_user.id = ms_report.id_user')
							->where('ms_report.idoffline', $id)
							->where('ms_report.delete', 0)
							->get()->getRow();
		$doc = json_decode($data->document);

		foreach ($doc as $key =>$value) {
			$_data[$data->idoffline.$key]['id']				= $data->idoffline.$key;
			$_data[$data->idoffline.$key]['name']			= $data->doc_type;
			$_data[$data->idoffline.$key]['tags']			= $data->doc_type;
			$_data[$data->idoffline.$key]['description']	= $data->description;
			$_data[$data->idoffline.$key]['doc_file']		= $value;
			$_data[$data->idoffline.$key]['user_owner']		= $data->user_owner;
			$_data[$data->idoffline.$key]['entry_date']		= $data->entry_date;
			$_data[$data->idoffline.$key]['is_public']		= 1;
		}
		
		return $_data;
	}


}
