<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
use App\Models\RoleModel;

class WorkflowModel extends Model
{
	protected $table			= 'tr_doc_matrix';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['name', 'approval', 'delete'];

	public $rm; //stands for role Model

	public function __construct(){
		$this->rm				= new RoleModel();
	}

	public function get_data($id = null){
		if ($id !== null) {
			$data = $this->where(['id' => $id])->get()->getRow();
			return $data;
		} else {
			$data = $this->where(['delete' => '0'])->orderBy('id', 'desc')->get()->getResult();
			return $data;
		}
	}

	public function create($data = null){
		$data['entry_date'] = date("Y-m-d H:i:s");
		$approval			= explode(',', $data['approval']);
		// unset($data['approval']); //remove from payload

		// saving data doc matrix
		$save = $this->save($data);
		if($save){
			$id_doc_matrix = $this->insertID();
			$db 		= db_connect('default'); 
			$builder 	= $db->table('tr_doc_matrix_approval');

			// saving data approval step doc matrix
			foreach ($approval as $value) {
				$id_role = $this->rm->get_role_id($value);
				if($id_role > 0)
					$builder->insert(array(	'id_doc_matrix' => $id_doc_matrix, 'id_role' => $id_role));
			}
		}
		return TRUE;
	}
	
	public function get_detail($id = null){
		if ($id == null) {
			return FALSE;
		} else {
			$db 		= db_connect('default'); 
			$builder 	= $db->table('tr_doc_matrix_approval');

			$data = $builder->select('tr_doc_matrix_approval.*, tb_roles.name role_name')
							->where('tr_doc_matrix_approval.id_doc_matrix', $id)
							->join('tb_roles', 'tb_roles.id = tr_doc_matrix_approval.id_role')
							->orderBy('tr_doc_matrix_approval.id', 'asc')->get()->getResult();

			return $data;
		}
	}


	public function get_data_option(){
		$data = $this->where(['delete' => '0'])
						->orderBy('name', 'asc')
						->get()->getResult();
		$options[null] = '----- Select Document Workflow -----';
		foreach ($data as $key => $value) {
			$options[$value->id] = $value->name;
		}

		// print_r($options);die;
		return $options;
	}
}
