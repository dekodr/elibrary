<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class RoleModel extends Model
{
	protected $table			= 'tb_roles';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['name', 'description', 'delete'];


	public function get_data($id = null){
		if ($id !== null) {
			$data = $this->where(['id' => $id])->get()->getRow();
			return $data;
		} else {
			$data = $this->where(['delete' => '0'])->get()->getResult();
			return $data;
		}
	}

	public function create($data = null){
		//saving data
		$data['entry_date'] = date("d-m-Y H:i:s");
		$this->save($data);
		return TRUE;
	}

	public function get_role_id($name = null){
		if ($name !== null) {
			$data = $this->where(['name' => $name])->get()->getRow();
			return $data->id;
		} else {
			return FALSE;
		}
	}

	public function get_data_option(){
		$data = $this->where(['delete' => '0'])
						->orderBy('name', 'asc')
						->get()->getResult();
		$options[null] = '----- Select User Role  -----';
		foreach ($data as $key => $value) {
			$options[$value->id] = $value->name;
		}

		// print_r($options);die;
		return $options;
	}
}
