<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
use App\Models\NotificationModel;

class DocModel extends Model
{
	protected $table			= 'ms_document';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['id_user','id_doc_matrix','name','description','tags','doc_type','doc_file','is_status','is_public','entry_date','edit_date','delete'];
	public $session;
	public $db;
	public $nm;

	public function __construct(){
		$this->session	= session();
		$this->db		= db_connect('default');
		$this->nm		= new NotificationModel();
	}

	// get related document
	public function get_data($id = null){

		if ($id !== null) {
			$data 					= $this->where(['id' => $id])->get()->getRow();
			$data->status			= $this->get_doc_status($data->id);
			$data->next_approval 	= $this->get_next_approval($data);
			return $data;
		} else {
			$id_user 	= $this->session->user['id'];
			$id_role 	= $this->session->user['id_role'];
			$data 		= $this ->select('ms_document.*')
								->join('tr_doc_matrix','ms_document.id_doc_matrix = tr_doc_matrix.id', 'LEFT')
								->join('tr_doc_matrix_approval','tr_doc_matrix.id = tr_doc_matrix_approval.id_doc_matrix', 'LEFT')
								->where('ms_document.delete', 0)
								->where('tr_doc_matrix_approval.id_role', $id_role)
								->orderBy('ms_document.id', 'desc')
								->get()->getResult();

			foreach ($data as $key => $value){
				$_data[$value->id] 					= $value;
				$_data[$value->id]->status			= $this->get_doc_status($value->id);
				$_data[$value->id]->user_owner		= $this->get_doc_owner($value->id_user);
				$_data[$value->id]->next_approval 	= $this->get_next_approval($value);
			}
			// print_r($data);
			return $_data;
		}
	}

	// get document user
	public function get_my_data(){
		$id_user 	= $this->session->user['id'];

		if($id_user !== null){
			$data = $this->select('ms_document.*, ms_user.name user_owner')
						->join('ms_user', 'ms_user.id = ms_document.id_user', 'LEFT')
						->where('id_user', $id_user)
						->where('ms_document.delete', 0)
						->orderBy('ms_document.id', 'desc')
						->get()->getResult();
			
			foreach ($data as $key => $value){
				$_data[$value->id] 					= $value;
				$_data[$value->id]->status			= $this->get_doc_status($value->id);
				$_data[$value->id]->user_owner		= $this->get_doc_owner($value->id_user);
				$_data[$value->id]->next_approval 	= $this->get_next_approval($value);
			}
			return $_data;
		}else{
			return FALSE;
		}
	}

	public function get_public_doc(){
		$data 		= $this ->select('ms_document.*')
							->join('tr_doc_matrix','ms_document.id_doc_matrix = tr_doc_matrix.id', 'LEFT')
							->join('tr_doc_matrix_approval','tr_doc_matrix.id = tr_doc_matrix_approval.id_doc_matrix', 'LEFT')
							->where('ms_document.delete', 0)
							->where('ms_document.is_public', 1)
							->where('ms_document.is_publish', 1)
							->orderBy('ms_document.id', 'desc')
							->get()->getResult();

		foreach ($data as $key => $value){
			$_data[$value->id] 					= $value;
			$_data[$value->id]->status			= $this->get_doc_status($value->id);
			$_data[$value->id]->user_owner		= $this->get_doc_owner($value->id_user);
		}

		return $_data;
	}

	// create new document
	public function create($payload = null){
		$payload['id_user'] 	= $this->session->user['id'];
		$payload['entry_date'] 	= date("Y-m-d H:i:s");

		// SAVE DOCUMENT
		$save = $this->save($payload);

		// create notification for next approval 

		return TRUE;
	}


	/* ----------------------------------------------------------------------------- */
	/* 									DOC STATUS 									 */
	/* ----------------------------------------------------------------------------- */

	function action_doc($id_doc, $status = 'approve', $doc_file = null, $to_user_owner = null, $detail = null){
		$document				= $this->get_data($id_doc);

		$to_user_owner			= ($to_user_owner == null && $document->is_status > 0)? $document->is_status - 1 : 0;
		$is_status				= ($status == 'reject')? $to_user_owner : $document->is_status + 1;
		$document->is_status	= $is_status;
		$approval				= $this->get_next_approval($document);
		$is_publish				= ($approval)? 0 : 1;


		$payload 	= array('is_status' => $is_status,
							'is_publish' => $is_publish,
							'edit_date' => date("Y-m-d H:i:s")
						);

		// update doc status
		$update		= $this->db->table('ms_document')->update($payload, ['id' => $id_doc]);

		// create doc status log
		$doc_status = $this->create_doc_status($id_doc, $status, $doc_file, $detail);

		// create notification for next approval
		
		return TRUE;
	}

	function create_doc_status($id_doc = null, $status = 'approve', $doc_file = null, $detail = null){
		if($id_doc !== null){
			$payload	= array(
							'id_document'	=> $id_doc,
							'id_user'		=> $this->session->user['id'],
							'description'	=> $status,
							'doc_file'		=> $doc_file,
							'detail'		=> $detail,
							'entry_date'	=> date("Y-m-d H:i:s"),
						);

			$data = $this->db->table('tr_doc_status')
							->insert($payload);
			return $data;
		}else{
			return FALSE;
		}
	}

	function get_doc_owner($id = null){
		$data = $this->db->table('ms_user')
						->select('ms_user.*')
						->where('ms_user.id', $id)
						->get()->getRow();

		return $data->name;
	}

	function get_doc_status($id = null){
		$data = $this->db->table('tr_doc_status')
						// ->join('ms_user', 'ms_user.id = tr_doc_stasus.id_user', 'LEFT')
						->join('ms_user','ms_user.id = tr_doc_status.id_user', 'LEFT')
						->select('ms_user.name, tr_doc_status.*')
						->where('tr_doc_status.id_document', $id)
						->orderBy('tr_doc_status.id', 'desc')
						->get()->getResult();
		// print_r($data);
		return $data;
	}

	function get_next_approval($doc = null){
		if($doc !== null){
			$matrix 		= $this->db->table('tr_doc_matrix_approval')
										->where('id_doc_matrix', $doc->id_doc_matrix)
										->get()->getResult();
										// print_r($doc->is_status);
			$next_approval	= $matrix[$doc->is_status];

			return $next_approval->id_role;
		}else{
			echo"false";
			return FALSE;
		}
	}

	function edit($id_doc, $payload){
		$payload['edit_date']	= date("Y-m-d H:i:s");
		$payload['is_reject']	= 0;
		$payload['delete']		= 0;

		$update		= $this->db->table('ms_document')->update($payload, ['id' => $id_doc]);

		return $update;
	}

	function remove($id_doc){
		$edit_date 	= date("Y-m-d H:i:s");
		$update		= $this->db->table('ms_document')->update(array('delete' => 1, 'edit_date' => $edit_date), ['id' => $id_doc]);
		// print_r($this->db->getLastQuery());die;
		return $update;
	}

	function revision($payload){
		$revision = $this->action_doc($payload['id_document'], 'reject', null, $payload['to_user_owner'], $payload['detail']);
		return TRUE;
	}

	/* ----------------------------------------------------------------------------- */
	/* 								DOC CATEGORY 									 */
	/* ----------------------------------------------------------------------------- */

	public function get_doc_category($q = 'mantap'){

		$id_user 	= $this->session->user['id'];
		$id_role 	= $this->session->user['id_role'];
		$tags		= array();
		
		if($q == null){
			$data	= $this ->select('ms_document.tags')
							->where('delete', 0)
							->get()->getResult();

			foreach ($data as $key => $value){
				$split = explode(",", $value->tags);

				foreach ($split as $split_){
					$tags[$split_]['id']	= $split_;
					$tags[$split_]['name']	= $split_;
					$tags[$split_]['url']	= base_url('doc/category?q='.$split_);
				}
			}

			$data_ = (object) $tags;
		}else{

			$data 		= $this ->select('ms_document.*')
								->join('tr_doc_matrix','ms_document.id_doc_matrix = tr_doc_matrix.id', 'LEFT')
								->join('tr_doc_matrix_approval','tr_doc_matrix.id = tr_doc_matrix_approval.id_doc_matrix', 'LEFT')
								->where('ms_document.delete', 0)
								// ->where('ms_document.is_public', 1)
								// ->where('ms_document.is_publish', 1)
								->like('tags', $q)
								->orderBy('ms_document.id', 'desc')
								->get()->getResult();
			
			$data_ = $this->doc_format($data);
		}


		return $data_;
	}

	// ----
	private function doc_format($data = null){

		if($data){
			foreach ($data as $key => $value){
				$_data[$value->id] 					= $value;
				$_data[$value->id]->status			= $this->get_doc_status($value->id);
				$_data[$value->id]->user_owner		= $this->get_doc_owner($value->id_user);
				$_data[$value->id]->next_approval 	= $this->get_next_approval($value);
			}
			return $_data;
		}else{
			return FALSE;
		}
	}
}
